import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.math.RoundingMode
import kotlin.math.pow

class Neuron(val neuronId: String, val layer: Layer) {
    private val logger: Logger = LoggerFactory.getLogger(javaClass)

    val incomingLinks: MutableList<Link> = mutableListOf()
    val outgoingLinks: MutableList<Link> = mutableListOf()
    var delta: Double = 0.0

    var activation: Double = 0.0

    init {
        logger.info("Initialized neuron $neuronId")
    }

    fun feedForward() {
        ingestInput()
        outgoingLinks.forEach {
            it.feedForward(activation)
        }
    }

    fun feedForward(input: Int) {
        activation = input.toDouble()
        outgoingLinks.forEach {
            it.feedForward(activation)
        }
    }

    fun feedForwardAndReturnOutput(): Double {
        ingestInput()
        activation = activation.toBigDecimal().setScale(2, RoundingMode.UP).toDouble()
        return activation
    }

    private fun ingestInput() {
        var tmpActivation = 0.0
        incomingLinks.forEach {
            tmpActivation += it.value
        }
        activation = sigmoid(tmpActivation)
    }

    private fun sigmoid(value: Double): Double {
        return 1 / (1 + Math.E.pow(-value))
    }

    fun calculateDelta() {
        delta = 0.0
        outgoingLinks.forEach {
            delta += it.toNeuron.delta * it.value
        }
        delta *= (activation * (1 - activation))
    }

    fun calculateDelta(target: Int) {
        delta = (-(target - activation)) * (activation * (1 - activation))
    }
}
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.system.exitProcess

private val logger: Logger = LoggerFactory.getLogger("main")
private lateinit var network: Network

fun main() {
    logger.info("Perceptron by Joonas Coatanea")
    logger.info("Enter the number of hidden layers(default 1):")
    val numberOfHiddenLayers: Int = readLine()?.toIntOrNull() ?: 1
    logger.info("Enter the amount of neurons per hidden layer(default 2):")
    val neuronsPerLayer: Int = readLine()?.toIntOrNull() ?: 2
    logger.info("Please enter the learning rate(default 0.5):")
    val learningRate: Double = readLine()?.toDoubleOrNull() ?: 0.5

    network = Network(neuronsPerLayer, numberOfHiddenLayers, learningRate)

    logger.info("Please choose what action to take:")
    logger.info("1 - To make a prediction using the network")
    logger.info("2 - To train the network")
    logger.info("3 - To quit")

    while (true) {
        print("> ")
        val choice: Int = readLine()?.toIntOrNull() ?: -1
        when (choice) {
            1 -> predict()
            2 -> train()
            3 -> exitProcess(0)
            else -> logger.warn("Please enter either 1, 2 or 3!")
        }
    }
}

fun predict() {
    logger.info("Please enter the input for the first input neuron(default 0):")
    val input1: Int = readLine()?.toIntOrNull() ?: 0
    logger.info("Please enter the input for the second input neuron(default 1):")
    val input2: Int = readLine()?.toIntOrNull() ?: 1
    val prediction: Double = network.feedForward(input1, input2)
    logger.info("Prediction: $prediction")
}

fun train() {
    logger.info("Please enter the amount of training rounds(default 10000:)")
    val rounds: Long = readLine()?.toLongOrNull() ?: 10000
    network.train(rounds)
}
enum class LayerType {
    INPUT, HIDDEN, OUTPUT;

    fun generateNeuronLayerId(): String {
        return when (this) {
            INPUT -> "ni"
            HIDDEN -> "nh"
            OUTPUT -> "no"
        }
    }
}
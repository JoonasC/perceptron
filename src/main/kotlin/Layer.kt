class Layer(neuronsPerLayer: Int, val layerType: LayerType) {
    val neurons: MutableList<Neuron> = mutableListOf()
    lateinit var connectedLayer: Layer

    init {
        if (State.lastLayerType != null) {
            if (State.lastNeuronIndex != 0 && State.lastLayerType != layerType) {
                State.lastNeuronIndex = 0
            }
        }
        repeat(neuronsPerLayer) {
            val neuronIndex: Int = State.lastNeuronIndex + 1
            val neuronId = "${layerType.generateNeuronLayerId()}$neuronIndex"
            State.lastNeuronIndex++
            neurons.add(Neuron(neuronId, this))
        }
        State.lastLayerType = layerType
    }

    fun feedForward(): Double {
        return when (layerType) {
            LayerType.HIDDEN -> {
                neurons.forEach {
                    it.feedForward()
                }
                connectedLayer.feedForward()
            }
            LayerType.OUTPUT -> {
                neurons.first().feedForwardAndReturnOutput()
            }
            else -> throw IllegalArgumentException("Unknown layer type: $layerType")
        }
    }

    fun feedForward(input1: Int, input2: Int): Double {
        neurons[0].feedForward(input1)
        neurons[1].feedForward(input2)
        return connectedLayer.feedForward()
    }

    fun connect(layer: Layer) {
        neurons.forEach { fromNeuron ->
            layer.neurons.forEach { toNeuron ->
                val link = Link("w${State.lastWeightIndex + 1}", fromNeuron, toNeuron)
                State.links.add(link)
                State.lastWeightIndex++
                fromNeuron.outgoingLinks.add(link)
                toNeuron.incomingLinks.add(link)
            }
        }
        connectedLayer = layer
    }

    fun calculateDeltas() {
        neurons.forEach {
            it.calculateDelta()
        }
    }

    fun calculateDeltas(target: Int) {
        neurons.forEach {
            it.calculateDelta(target)
        }
    }
}
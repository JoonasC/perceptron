import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.math.RoundingMode
import java.util.concurrent.ThreadLocalRandom

class Link(val linkId: String, val fromNeuron: Neuron, val toNeuron: Neuron) {
    private val logger: Logger = LoggerFactory.getLogger(javaClass)

    var weight: Double =
        ThreadLocalRandom.current().nextDouble(-1.0, 1.0).toBigDecimal().setScale(2, RoundingMode.UP).toDouble()
    var value: Double = 0.0

    init {
        logger.info("Initialized link $linkId: $weight")
    }

    fun calculateCorrection(): Double {
        return toNeuron.delta * fromNeuron.activation
    }

    fun feedForward(value: Double) {
        this.value = value * weight
    }
}
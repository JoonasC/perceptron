object State {
    var lastWeightIndex: Int = 0
    var lastNeuronIndex: Int = 0
    var lastLayerType: LayerType? = null
    val links: MutableList<Link> = mutableListOf()
}
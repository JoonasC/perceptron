import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.math.pow

class Network(neuronsPerLayer: Int, numberOfHiddenLayers: Int, private val learningRate: Double) {
    private val logger: Logger = LoggerFactory.getLogger(javaClass)

    val inputLayer: Layer
    val hiddenLayers: MutableList<Layer> = mutableListOf()
    val outputLayer: Layer

    init {
        inputLayer = Layer(2, LayerType.INPUT)
        repeat(numberOfHiddenLayers) {
            hiddenLayers.add(Layer(neuronsPerLayer, LayerType.HIDDEN))
        }
        outputLayer = Layer(1, LayerType.OUTPUT)
        inputLayer.connect(hiddenLayers.first())
        for (i in (0..(hiddenLayers.size - 2))) {
            hiddenLayers[i].connect(hiddenLayers[i + 1])
        }
        hiddenLayers.last().connect(outputLayer)
    }

    fun feedForward(input1: Int, input2: Int): Double {
        return inputLayer.feedForward(input1, input2)
    }

    fun train(rounds: Long) {
        logger.info("Training...")
        val errors: MutableList<Double> = mutableListOf()
        var i = 0
        var i2 = 0
        while (i < rounds) {
            val input1: Int
            val input2: Int
            val target: Int
            when (i2) {
                0 -> {
                    input1 = 0
                    input2 = 0
                    target = 0
                }
                1 -> {
                    input1 = 0
                    input2 = 1
                    target = 1
                }
                2 -> {
                    input1 = 1
                    input2 = 0
                    target = 1
                }
                3 -> {
                    input1 = 1
                    input2 = 1
                    target = 0
                    i2 = -1
                }
                else -> {
                    input1 = 0
                    input2 = 0
                    target = 0
                }
            }
            i2++
            logger.info("Input 1: $input1")
            logger.info("Input 2: $input2")
            logger.info("Target: $target")
            val prediction: Double = inputLayer.feedForward(input1, input2)
            logger.info("Prediction: $prediction")
            val error: Double = calculateError(target, prediction)
            errors.add(error)
            outputLayer.calculateDeltas(target)
            hiddenLayers.reversed().forEach {
                it.calculateDeltas()
            }
            val linksAndCorrections: List<Pair<Link, Double>> =
                State.links.reversed().map { Pair(it, it.calculateCorrection()) }
            linksAndCorrections.forEach {
                it.first.weight -= learningRate * it.second
            }
            i++
        }
        logger.info("Average loss: ${errors.sum() / errors.size}")
    }

    fun calculateError(target: Int, prediction: Double): Double {
        return 0.5 * (target - prediction).pow(2)
    }
}